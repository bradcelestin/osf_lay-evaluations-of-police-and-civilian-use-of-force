
model{
for( rowIdx in 1:nDataRows ) {
  y[rowIdx] ~ dnorm(
    b0[action[rowIdx]] + b1*fona[rowIdx] ,
    1/Sigma^2)
  yBin[rowIdx] ~ dinterval( y[rowIdx] , threshMat[rowIdx,] )
} # ends model defining loop

# Priors
for( pairTypeIdx in 1:nActPairs ) {
  b0[pairTypeIdx] ~ dnorm( 0 , 1/10^2 )
   } # ends prior for loop
  b1 ~ dnorm( 0 , 1/10^2 )
  Sigma ~ dunif( 0 , 5 )
} # close model string

