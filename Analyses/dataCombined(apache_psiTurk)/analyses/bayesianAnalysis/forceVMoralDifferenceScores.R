# Optional generic preliminaries:
graphics.off() # This closes all of R's graphics windows.
rm(list=ls())  # Careful! This clears all of R's memory!

#--------------------------------------------------------------------
# Change these things 
#--------------------------------------------------------------------
# Set working directory
wd = "~/Dropbox/Grad School/Experiments/Police_UseOfForce/jsPsych_Experiments/UseOfForce_ExtendedActionList/dataCombined(apache_psiTurk)/analyses/bayesianAnalysis/"
setwd(wd)
# Load the DBDA Utilities
source("DBDA2Eprograms_V.23/DBDA2E-utilities_PUoF.R") # in a folder one level up from the wd

# # File name root for saved output files:
# dirName="PUoF-Jags-Indiv-ForceVMoral-DifferenceScores"
# dir.create(dirName)
# fileNameRoot=paste0(dirName) # For output file names.
# graphFileType="png"

# Load the combined data: 
theData = read.csv("~/Dropbox/Grad School/Experiments/Police_UseOfForce/jsPsych_Experiments/UseOfForce_ExtendedActionList/dataCombined(apache_psiTurk)/data/combinedData.csv")

attach(theData)
# Reorder the civActName and offActName factor levels so that the two fixed actions are in the first and last positions
civNameVec = levels(civActName) # extract all levels of the actions
offNameVec = levels(offActName)

leastSevereCiv = which(civNameVec=="waved")
civNameVec = c("waved", civNameVec[-leastSevereCiv])
mostSevereCiv = which(civNameVec=="handgunHead")
civNameVec = c(civNameVec[-mostSevereCiv], "handgunHead")

leastSevereOff = which(offNameVec=="waved")
offNameVec = c("waved", offNameVec[-leastSevereOff])
mostSevereOff = which(offNameVec=="handgunHead")
offNameVec = c(offNameVec[-mostSevereOff], "handgunHead")
detach(theData)

# Use the newly ordered vectors to change the factor order in theData
theData$civActName = factor(theData$civActName, levels=civNameVec, order=TRUE)
theData$offActName = factor(theData$offActName, levels=offNameVec, order=TRUE)

#Do the same for the civActName and offActName so they can be indexed:
theData = cbind(theData, civActIdx = as.numeric(theData[,"civActName"]))
theData = cbind(theData, offActIdx = as.numeric(theData[,"offActName"]))

# Read in parameter estimates from the force and moral models
forceSummary = read.csv("./PUoF-Jags-Indiv-ForceQ/PUoF-Jags-Indiv-ForceQ-SummaryInfo.csv")
moralSummary = read.csv("./PUoF-Jags-Indiv-AcceptAppropPunishQs/PUoF-Jags-Indiv-AcceptAppropPunishQs-SummaryInfo.csv")

# Change the first column names from X to parameterNames for clarity
names(forceSummary) = sub('X','parameterNames', names(forceSummary)) 
names(moralSummary) = sub('X','parameterNames', names(moralSummary))

# Read the officer and civilian action levels in the order that R evaluated them for the models so you can match parameter names to action names
offActLevels = levels(theData$offActName)
civActLevels = levels(theData$civActName)

# Select only the officer action scale estimate rows
forceOffActs = forceSummary[grep("offActScale",forceSummary$parameterNames) , ]
moralOffActs = moralSummary[grep("offActScale",moralSummary$parameterNames) , ]
forceCivActs = forceSummary[grep("civActScale",forceSummary$parameterNames) , ]
moralCivActs = moralSummary[grep("civActScale",moralSummary$parameterNames) , ]

# Add the action names in a column
forceOffActs$actName = offActLevels
moralOffActs$actName = offActLevels
forceCivActs$actName = civActLevels
moralCivActs$actName = civActLevels

# Calculate difference scores; force - moral
differenceScoresOff = data.frame(forceOffActs$Mode - moralOffActs$Mode)
differenceScoresOff$actName = offActLevels

differenceScoresCiv = data.frame(forceCivActs$Mode - moralCivActs$Mode)
differenceScoresCiv$actName = civActLevels

# Load the spreadsheet containing 
spreadsheet = read.csv("~/Dropbox/Grad School/Experiments/Police_UseOfForce/jsPsych_Experiments/ExplicitNormativity/normativeOffActs.csv" , header = TRUE)

spreadsheet$policeNormCont = differenceScoresOff$forceOffActs.Mode...moralOffActs.Mode[match(spreadsheet$Original.Short.Description , differenceScoresOff$actName) ]

for ( idx in 1:nrow(spreadsheet)){
  if (is.na(spreadsheet$policeNormCont[idx])){
    spreadsheet$policeNormCont[idx] = differenceScoresCiv$forceCivActs.Mode...moralCivActs.Mode[match(spreadsheet$Original.Short.Description[idx] , differenceScoresCiv$actName)]
  }
}
                                                                                    
write.csv(spreadsheet , file = "~/Dropbox/Grad School/Experiments/Police_UseOfForce/jsPsych_Experiments/ExplicitNormativity/normativeOffActs_forceVMoralDiffScores.csv")

#--------------------------------------------------------------------
# Analysis
#--------------------------------------------------------------------

# Correlation between the "normativity" of latent estimates and the # of syllables in the short title
cor.test(spreadsheet$policeNormCont , spreadsheet$X..of.Syllables..short.title.)

# Correlation between the "normativity" of latent estimates and the # of characters in the short title
cor.test(spreadsheet$policeNormCont , spreadsheet$X..of.Characters..short.title.)
