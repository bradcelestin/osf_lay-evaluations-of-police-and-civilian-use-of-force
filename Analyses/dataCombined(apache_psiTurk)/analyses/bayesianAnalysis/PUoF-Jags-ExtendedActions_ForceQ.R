
# PUoF-Jags.R

# Optional generic preliminaries:
graphics.off() # This closes all of R's graphics windows.
rm(list=ls())  # Careful! This clears all of R's memory!

#--------------------------------------------------------------------
# Change these things 
#--------------------------------------------------------------------
# Set working directory
wd = "~/Dropbox/Grad School/Experiments/Police_UseOfForce/jsPsych_Experiments/UseOfForce_ExtendedActionList/dataCombined(apache_psiTurk)/analyses/bayesianAnalysis/"
setwd(wd)
# Load the DBDA Utilities
source("DBDA2Eprograms_V.23/DBDA2E-utilities_PUoF.R") # in a folder one level up from the wd
# Load the data:
theData = read.csv("~/Dropbox/Grad School/Experiments/Police_UseOfForce/jsPsych_Experiments/UseOfForce_ExtendedActionList/dataCombined(apache_psiTurk)/data/combinedData.csv")

# Load the functions used below:
require(lsr)
require(runjags)

# File name root for saved output files:
dirName="PUoF-Jags-Indiv-ForceQ"
dir.create(dirName)
fileNameRoot=paste0(dirName) # For output file names.
graphFileType="png"


#..............................................................................

# Convert feltObligation and normAlignment to standardized average, called fona:
standardize = function( y ) {
  return( ( y - mean(y) ) / sqrt( mean( (y-mean(y))^2 ) ) )
}
zFO = standardize( theData$feltObligation )
zNA = standardize( theData$normAlignment )
fona = ( zFO + zNA ) / 2
theData = cbind( theData , fona = fona )

# Remove dubious subjects:
# Check the "manipulation check" responses and exclude bad subjects:
# *** TO BE DONE LATER ***

# Delete manipCheckLow and manipCheckHigh rows, 
# also delete those levels from the factor:
theData = theData[ substr(theData$questionName,1,10)!="manipCheck" , ]
theData$questionName = factor( theData$questionName )

# Extract only the psycho-social questions (not the physical harm questions):
# and re-establish remaining levels of the factor:
theData = theData[ substr(theData$questionName,1,5)=="force" , ]
theData$questionName = factor( theData$questionName )

# For ease of processing, select only subset of relevant columns:
subData = theData[ , c( "randSubjectID" ,
                        "questionName" ,
                        "civActName" , 
                        "offActName" , 
                        "fona" ,
                        "response" ) ] 
# Replace long subject ID with consecutive subject index:
subData[,"randSubjectID"] = as.numeric(factor(subData[,"randSubjectID"]))

attach(subData)
# Reorder the civActName and offActName factor levels so that the two fixed actions are in the first and last positions
civNameVec = levels(civActName) # extract all levels of the actions
offNameVec = levels(offActName)

leastSevereCiv = which(civNameVec=="waved")
civNameVec = c("waved", civNameVec[-leastSevereCiv])
mostSevereCiv = which(civNameVec=="handgunHead")
civNameVec = c(civNameVec[-mostSevereCiv], "handgunHead")

leastSevereOff = which(offNameVec=="waved")
offNameVec = c("waved", offNameVec[-leastSevereOff])
mostSevereOff = which(offNameVec=="handgunHead")
offNameVec = c(offNameVec[-mostSevereOff], "handgunHead")
detach(subData)

# Use the newly ordered vectors to change the factor order in subData
subData$civActName = factor(subData$civActName, levels=civNameVec, order=TRUE)
subData$offActName = factor(subData$offActName, levels=offNameVec, order=TRUE)

#Do the same for the civActName and offActName so they can be indexed:
subData = cbind(subData, civActIdx = as.numeric(subData[,"civActName"]))
subData = cbind(subData, offActIdx = as.numeric(subData[,"offActName"]))

# Convert question name to question index and append column:
subData = cbind( subData , questionIdx = as.numeric(subData[,"questionName"] ) )

# N.B.: response goes from 1 to 100, not from 0 to 100, but is centered at 
# 50. Hence 1 and 99 are symmetric around 50, but responses go from 1 to 100, 
# leaving the logitResp asymmetric. Therefore change responses of 100 to 99,
# so that logit is symmetric.
subData[ subData[,"response"]==100 ,"response"] = 99

# Transform ratings from 0-100 scale to logit scale. First squeeze the ratings
# away from 0/1 extremes, so logit does not explode.
smidge = 0.01
squeezeResp = (1.0-smidge)*(subData[,"response"]/100) + (smidge/2)
logitResp = log( squeezeResp / ( 1.0 - squeezeResp ) )
subData = cbind( subData , logitResp )

# Convert extreme responses to "censored" values.
# Specify censoring limits:
censorHalfWidth = 45
censorLim = 50 + c( -censorHalfWidth , censorHalfWidth )
squeezeCensorLim = (1.0-smidge)*(censorLim/100) + (smidge/2)
logitCensorLim = log( squeezeCensorLim / ( 1.0 - squeezeCensorLim ) )
# Identify the rows of the data that are censored, label each row with its
# response bin for JAGS, and create new vectors of censored data:
censoredLowRows = ( subData[,"response"] <= censorLim[1] )
censoredHighRows = ( subData[,"response"] >= censorLim[2] )
respBin = rep(1,length=nrow(subData))
respBin[ censoredLowRows ] = 0
respBin[ censoredHighRows] = 2
censorResp = rep(NA,length=nrow(subData))
censorResp[respBin==1] = subData[respBin==1,"response"]
logitCensorResp = rep(NA,length=nrow(subData))
logitCensorResp[respBin==1] = subData[respBin==1,"logitResp"]
# Assemble the censored data and respBin into the data frame:
subData = cbind( subData , 
                 respBin = respBin ,
                 censorResp = censorResp ,
                 censorLim1 = rep( censorLim[1] , nrow(subData) ) ,
                 censorLim2 = rep( censorLim[2] , nrow(subData) ) ,
                 logitCensorResp = logitCensorResp ,
                 logitCensorLim1 = rep( logitCensorLim[1] , nrow(subData) ) ,
                 logitCensorLim2 = rep( logitCensorLim[2] , nrow(subData) ) )

#----------------------------------------------------------------------
# Create table containing the cell N for punishment question civAct/offAct combination -- this will be the same N as the other questions, and both punishCiv and punishOff must be included since we only asked about one or the other per scenario
if(FALSE){
cellDataforce = subData[questionName=="forceOff" | questionName=="forceCiv",]
cellDataforce$questionName = factor(cellDataforce$questionName)
cellNTable = table(cellDataforce$questionName, cellDataforce$civActName, cellDataforce$offActName)
cellNTable = data.frame(cellNTable) # convert the table to a data.frame
names(cellNTable) = c("questionName","civActName","offActName","cellN") # rename the columns
cellNTable[cellNTable$cellN==1,] # View the cells that only have one response
summary(cellNTable$cellN) # summary stats for cell N
openGraph(width=8, height=8)
plot(cellNTable$cellN,
     main="Cell N for every action pair: all force off/civ questions",
     ylim=c(0,20),
     yaxt="n",
     # pch=cellNTable$cellN,
     col=cellNTable$cellN,
     ylab = "N per action pair")
axis(2, at=seq(0,18, by=2))
text( (nrow(cellNTable)/2) ,
      (max(unique(cellNTable$cellN)+1.5)) ,
      labels=bquote( list(
        median==.(median(cellNTable$cellN)) , 
        mean==.(round(mean(cellNTable$cellN),2)) , 
        range==.(range(cellNTable$cellN)[1]):.(range(cellNTable$cellN)[2])) )  )
saveGraph( file=paste0(fileNameRoot,"-totalCellN") , type=graphFileType )
}

# For debugging, use only a few subjects:
rowsPerSubj = sum(subData$randSubjectID==1)
nSubject = nrow(subData)/rowsPerSubj
nSubjToUse = nSubject # 10-ish or nSubject
subData = subData[ 1:(nSubjToUse*rowsPerSubj) , ]

# Make columns of subData available as variables:
attach( subData )

# Histograms of ratings:
if (FALSE) {
  histwnorm = function( y , endBinLim , endBinCol="red" ,  ... ) {
    histInfo = hist( y , probability=TRUE , breaks=21 ,
                     col="grey" , border="white" , ... )
    abline(v=endBinLim[1],lty="dashed",col=endBinCol)
    abline(v=endBinLim[2],lty="dashed",col=endBinCol)
    xvec = seq( endBinLim[1] , endBinLim[2] , length.out=201 )
    yInLim = y[ y > endBinLim[1] & y < endBinLim[2] ]
    pInLim = length( yInLim ) / length(y)
    lines( xvec , pInLim*dnorm( xvec , mean=mean(yInLim) , sd=sd(yInLim) ) ,
           col=endBinCol )
  }
  # Create histograms for all ratings for each question
  for(qName in levels(subData$questionName)){
    openGraph(height=7,width=5)
    includeRows = subData$questionName==qName
    layout(matrix(1:2,nrow=2))
    endBinLim100 = censorLim
    endBinLimLogit = logitCensorLim
    histwnorm( subData[includeRows,"response"] , 
               xlim=c(0,100) , endBinLim=endBinLim100 ,
               main=bquote( list( Question==.(qName) ) ) )
    histwnorm( subData[includeRows,"logitResp"] ,
               xlim=c(-4.5,4.5) , endBinLim=endBinLimLogit ,
               main=bquote( list( Question==.(qName) ) ) )
    saveGraph( file=paste0(dirName,"/",fileNameRoot,"-DataHist",
                           "-Q",qName) , type=graphFileType )
  }
}
#graphics.off()
warning("** See other graphs saved in folder. **")

#..............................................................................
# Assemble the data for JAGS:

dataList = list(    # Put the information into a list.
  nDataRows = length(subData$questionIdx), 
  civAct=subData$civActIdx,
  offAct=subData$offActIdx,
  civActScale=c(1,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,6)-mean(1:6),
  offActScale=c(1,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,6)-mean(1:6),
  q = subData$questionIdx ,
  nQ = max(subData$questionIdx) ,
  fona = subData$fona ,
  y = subData$logitCensorResp ,
  yBin = subData$respBin ,
  threshMat = as.matrix(subData[,c("logitCensorLim1","logitCensorLim2")]) 
)

#..............................................................................
# Define the model:
modelString = "
model {
  for ( rowIdx in 1:nDataRows ) {
    y[rowIdx] ~ dnorm( 
            B0[q[rowIdx]] 
          + BCivAct[q[rowIdx]] * civActScale[civAct[rowIdx]]
          + BOffAct[q[rowIdx]] * offActScale[offAct[rowIdx]] 
          + Bfona[q[rowIdx]] * fona[rowIdx]
          + BfonaCivAct[q[rowIdx]] * civActScale[civAct[rowIdx]] * fona[rowIdx]
          + BfonaOffAct[q[rowIdx]] * offActScale[offAct[rowIdx]] * fona[rowIdx] 
          , 1/Sigma[q[rowIdx]]^2 )
    yBin[rowIdx] ~ dinterval( y[rowIdx] , threshMat[rowIdx,] )
  }
  for ( qIdx in 1:nQ ) {
    B0[qIdx]          ~ dnorm( 0 , 1/10^2 )
    BCivAct[qIdx]     ~ dnorm( 0 , 1/10^2 )
    BOffAct[qIdx]     ~ dnorm( 0 , 1/10^2 )
    Bfona[qIdx]       ~ dnorm( 0 , 1/10^2 ) # <- 0.0
    BfonaCivAct[qIdx] ~ dnorm( 0 , 1/10^2 ) # <- 0.0
    BfonaOffAct[qIdx] ~ dnorm( 0 , 1/10^2 ) # <- 0.0
    Sigma[qIdx]       ~ dunif( 0 , 5 )
  }
  for ( actIdx in 2:21 ) {
    civActScale[actIdx] ~ dnorm( 0 , 1/5^2 ) # 3.5 is mean(1:6)
    offActScale[actIdx] ~ dnorm( 0 , 1/5^2 ) # 3.5 is mean(1:6)
  }
}
" # close quote for modelString
# Save JAGS model to file:
modelFileName = paste0(dirName,"/",fileNameRoot,"-Model.txt")
writeLines( modelString , con=modelFileName )

#..............................................................................
# Run the chains:
if (FALSE) {
  parameters = c( "B0","BCivAct","BOffAct",
                  "Bfona","BfonaCivAct","BfonaOffAct",
                  "Sigma","civActScale","offActScale" )
  runJagsOut <- run.jags( method="parallel" ,
                          model=modelFileName , 
                          monitor=parameters , 
                          data=dataList ,  
                          #inits=initsList , 
                          n.chains=nChainsDefault , # from DBDA2E utilities
                          adapt=500 ,
                          burnin=1000 , # 1000
                          sample=ceiling(12000/nChainsDefault) , # 12000
                          thin=3 ,
                          summarise=FALSE ,
                          plots=FALSE )
  codaSamples = as.mcmc.list( runJagsOut )
  save( codaSamples , file=paste0(dirName,"/",fileNameRoot,"-Mcmc.Rdata") )
} else {
  load(file=paste0(dirName,"/",fileNameRoot,"-Mcmc.Rdata"))
}

#..............................................................................
# Examine the chains:
mcmcMat = as.matrix(codaSamples)
parameterNames = colnames(mcmcMat)

# Convergence diagnostics:
if (FALSE) {
  for ( parName in parameterNames ) {
    diagMCMC( codaObject=codaSamples , parName=parName , 
              saveName=fileNameRoot , saveType=graphFileType )
  }
  graphics.off()
}

# Summary table:
if (TRUE) {
  summaryInfo = NULL
  for ( parName in parameterNames ) {
    summaryInfo = rbind( summaryInfo , summarizePost( mcmcMat[,parName] , compVal=0.0 ) )
  }
  rownames(summaryInfo) = parameterNames
  show(round(summaryInfo[,1:9],2))
  write.csv( summaryInfo , file=paste0(dirName,"/",fileNameRoot,"-SummaryInfo.csv") )
}

#..............................................................................
# Plot action scale values for civilian and officer actions separately
#---------------Civilian-------------------------------------------------------
nActions=22
openGraph(height=8,width=8)
layout( matrix( c(1:20, 21,22 , 0,0), nrow=6 , byrow=TRUE) )
par( mar=c(3.5,0.5,2.5,0.5) , mgp=c(2.25,0.7,0) )
civActScaleMo = rep(NA,nActions)
actionNames = data.frame(levels(subData$civActName))
for(actNIdx in 1:nrow(actionNames)){
  actionNames$scaleID[actNIdx] = paste0("civActScale[",actNIdx,"]")
}
colnames(actionNames) = c("actionName","scaleID")
scaleOnly = data.frame(summaryInfo[actionNames$scaleID,])
actionNames$posteriorMode = scaleOnly$Mode[match(actionNames$scaleID,rownames(scaleOnly))]
actionNames = sortFrame(actionNames, posteriorMode)

# Loop through actionNames and make plots
for ( actIdx in 1:nrow(actionNames) ) {
  parName = actionNames$scaleID[actIdx]
  postInfo = plotPost( mcmcMat[,parName] , 
                       main=paste0("Civ: ",actionNames$actionName[actIdx]) , cex.main=1.5 ,
                       xlab="Scale Value" , xlim=c(-3,3) , 
                       border="skyblue" , HDItextPlace = 0.95 )
  box(which="figure")
  civActScaleMo[actIdx] = postInfo[1,"mode"]
}
saveGraph( file=paste0(dirName,"/",fileNameRoot,"CivActScale") , type=graphFileType )

#------------Officer-----------------------------------------------------------
nActions=22
openGraph(height=8,width=8)
layout( matrix( c(1:20, 21,22 , 0,0), nrow=6 , byrow=TRUE) )
par( mar=c(3.5,0.5,2.5,0.5) , mgp=c(2.25,0.7,0) )
offActScaleMo = rep(NA,nActions)
actionNames = data.frame(levels(subData$offActName))
for(actNIdx in 1:nrow(actionNames)){
  actionNames$scaleID[actNIdx] = paste0("offActScale[",actNIdx,"]")
}
colnames(actionNames) = c("actionName","scaleID")
scaleOnly = data.frame(summaryInfo[actionNames$scaleID,])
actionNames$posteriorMode = scaleOnly$Mode[match(actionNames$scaleID,rownames(scaleOnly))]
actionNames = sortFrame(actionNames, posteriorMode)

# Loop through actionNames and make plots
for ( actIdx in 1:nrow(actionNames) ) {
  parName = actionNames$scaleID[actIdx]
  postInfo = plotPost( mcmcMat[,parName] , 
                       main=paste0("Off: ",actionNames$actionName[actIdx]) , cex.main=1.5 ,
                       xlab="Scale Value" , xlim=c(-3,3) , 
                       border="skyblue" , HDItextPlace = 0.95 )
  box(which="figure")
  offActScaleMo[actIdx] = postInfo[1,"mode"]
}
saveGraph( file=paste0(dirName,"/",fileNameRoot,"OffActScale") , type=graphFileType )

#..............................................................................
# Plot civilian x officer action scale values with HDIs
#------------------------------------------------------------------------------
offActContCat = data.frame(levels(subData$offActName))
civActContCat = data.frame(levels(subData$civActName))
colnames(offActContCat) = c("offActionName")
colnames(civActContCat) = c("civActionName")

offActContCat$offForceLevel[which(offActContCat$offActionName == "waved")] = 1 
offActContCat$offForceLevel[which(offActContCat$offActionName == "asked")] = 1
offActContCat$offForceLevel[which(offActContCat$offActionName == "stoodNear")] = 1
offActContCat$offForceLevel[which(offActContCat$offActionName == "getBack")] = 2
offActContCat$offForceLevel[which(offActContCat$offActionName == "handsUp")] = 2
offActContCat$offForceLevel[which(offActContCat$offActionName == "hurtThreat")] = 2
offActContCat$offForceLevel[which(offActContCat$offActionName == "nameCall")] = 2
offActContCat$offForceLevel[which(offActContCat$offActionName == "handcuff")] = 3
offActContCat$offForceLevel[which(offActContCat$offActionName == "pepperSpray")] = 3
offActContCat$offForceLevel[which(offActContCat$offActionName == "tazer")] = 3
offActContCat$offForceLevel[which(offActContCat$offActionName == "armbar")] = 3
offActContCat$offForceLevel[which(offActContCat$offActionName == "punchFace")] = 4
offActContCat$offForceLevel[which(offActContCat$offActionName == "kickStom")] = 4
offActContCat$offForceLevel[which(offActContCat$offActionName == "headbutt")] = 4
offActContCat$offForceLevel[which(offActContCat$offActionName == "flashlight")] = 5
offActContCat$offForceLevel[which(offActContCat$offActionName == "choke")] = 5
offActContCat$offForceLevel[which(offActContCat$offActionName == "beanbag")] = 5
offActContCat$offForceLevel[which(offActContCat$offActionName == "batonLeg")] = 5
offActContCat$offForceLevel[which(offActContCat$offActionName == "headSlam")] = 6
offActContCat$offForceLevel[which(offActContCat$offActionName == "vehicle")] = 6
offActContCat$offForceLevel[which(offActContCat$offActionName == "handgunChest")] = 6
offActContCat$offForceLevel[which(offActContCat$offActionName == "handgunHead")] = 6

civActContCat$civForceLevel[which(civActContCat$civActionName == "waved")] = 1 
civActContCat$civForceLevel[which(civActContCat$civActionName == "asked")] = 1
civActContCat$civForceLevel[which(civActContCat$civActionName == "stoodNear")] = 1
civActContCat$civForceLevel[which(civActContCat$civActionName == "getBack")] = 2
civActContCat$civForceLevel[which(civActContCat$civActionName == "dontListen")] = 2
civActContCat$civForceLevel[which(civActContCat$civActionName == "hurtThreat")] = 2
civActContCat$civForceLevel[which(civActContCat$civActionName == "nameCall")] = 2
civActContCat$civForceLevel[which(civActContCat$civActionName == "pulledAway")] = 3
civActContCat$civForceLevel[which(civActContCat$civActionName == "pepperSpray")] = 3
civActContCat$civForceLevel[which(civActContCat$civActionName == "tazer")] = 3
civActContCat$civForceLevel[which(civActContCat$civActionName == "armbar")] = 3
civActContCat$civForceLevel[which(civActContCat$civActionName == "punchFace")] = 4
civActContCat$civForceLevel[which(civActContCat$civActionName == "kickStom")] = 4
civActContCat$civForceLevel[which(civActContCat$civActionName == "headbutt")] = 4
civActContCat$civForceLevel[which(civActContCat$civActionName == "flashlight")] = 5
civActContCat$civForceLevel[which(civActContCat$civActionName == "choke")] = 5
civActContCat$civForceLevel[which(civActContCat$civActionName == "ballBat")] = 5
civActContCat$civForceLevel[which(civActContCat$civActionName == "metalPipe")] = 5
civActContCat$civForceLevel[which(civActContCat$civActionName == "headSlam")] = 6
civActContCat$civForceLevel[which(civActContCat$civActionName == "vehicle")] = 6
civActContCat$civForceLevel[which(civActContCat$civActionName == "handgunChest")] = 6
civActContCat$civForceLevel[which(civActContCat$civActionName == "handgunHead")] = 6

for(actNIdx in 1:nrow(offActContCat)){
  offActContCat$scaleID[actNIdx] = paste0("offActScale[",actNIdx,"]")
}
for(actNIdx in 1:nrow(civActContCat)){
  civActContCat$scaleID[actNIdx] = paste0("civActScale[",actNIdx,"]")
}

scaleOnlyOff = data.frame(summaryInfo[offActContCat$scaleID,])
scaleOnlyCiv = data.frame(summaryInfo[civActContCat$scaleID,])
offActContCat$posteriorMedian = scaleOnlyOff$Median[match(offActContCat$scaleID,rownames(scaleOnlyOff))]
offActContCat$posteriorMode = scaleOnlyOff$Mode[match(offActContCat$scaleID,rownames(scaleOnlyOff))]
offActContCat$HDIlow = scaleOnlyOff$HDIlow[match(offActContCat$scaleID,rownames(scaleOnlyOff))]
offActContCat$HDIhigh = scaleOnlyOff$HDIhigh[match(offActContCat$scaleID,rownames(scaleOnlyOff))]
civActContCat$posteriorMedian = scaleOnlyCiv$Median[match(civActContCat$scaleID,rownames(scaleOnlyCiv))]
civActContCat$posteriorMode = scaleOnlyCiv$Mode[match(civActContCat$scaleID,rownames(scaleOnlyCiv))]
civActContCat$HDIlow = scaleOnlyCiv$HDIlow[match(civActContCat$scaleID,rownames(scaleOnlyCiv))]
civActContCat$HDIhigh = scaleOnlyCiv$HDIhigh[match(civActContCat$scaleID,rownames(scaleOnlyCiv))]
# combinedFrame = cbind(offActContCat , civActContCat) # Check to make sure the numbers are correct

# Change the factor levels of the actions so that the officer and civilian rows line up

# Reorder the civActName and offActName factor levels so that the two fixed actions are in the first and last positions
civNameVec = c('stoodNear','asked','waved','getBack','dontListen','hurtThreat','nameCall','pulledAway','pepperSpray','tazer','armbar','punchFace','kickStom','choke','headbutt','metalPipe','ballBat','flashlight','handgunChest','handgunHead','vehicle','headSlam')
offNameVec = c('stoodNear','asked','waved','getBack','handsUp','hurtThreat','nameCall','handcuff','pepperSpray','tazer','armbar','punchFace','kickStom','choke','headbutt','batonLeg','beanbag','flashlight','handgunChest','handgunHead','vehicle','headSlam')
combinedNameVec = c('stoodNear','asked','waved','getBack','dontListen/handsUp','hurtThreat','nameCall','pulledAway/handcuff','pepperSpray','tazer','armbar','punchFace','kickStom','choke','headbutt','pipeLeg/batonLeg','ballBat/beanbag','flashlight','handgunChest','handgunHead','vehicle','headSlam')

# Use the newly ordered vectors to change the factor order in subData
offActContCat$offActionName = factor(offActContCat$offActionName, levels=offNameVec, order=TRUE)
civActContCat$civActionName = factor(civActContCat$civActionName, levels=civNameVec, order=TRUE)

# offActContCat$offActionName = reorder(offActContCat$offActionName, new.order=offNameVec)
# civActContCat$civActionName = reorder(civActContCat$civActionName, new.order=civNameVec)

# Sort the off and civ dataframes so that the actions are in the order of the action name factor levels
offActContCat = offActContCat[order(offActContCat$offActionName),]
civActContCat = civActContCat[order(civActContCat$civActionName),]
rownames(offActContCat) = 1:nrow(offActContCat)
rownames(civActContCat) = 1:nrow(civActContCat)

# Create a vector for colors for each canonical force continuum category
plotCols = c("purple","blue","green4","yellow3","orange","red")
plotCols = plotCols[offActContCat$offForceLevel]

# Designate label positions
posVector = rep(2, length(combinedNameVec))
posVector[combinedNameVec %in% c("handgunHead", "headSlam", "vehicle","pipeLeg/batonLeg")] = 3
posVector[combinedNameVec %in% c("headbutt","choke","ballBat/beanbag", "punchFace", "asked", "pulledAway/handcuff","getBack")] = 4
posVector[combinedNameVec %in% c( "kickStom","pepperSpray","armbar")] = 1

#-------------------------------------------------------------------------------
# Plot of points and action names
#-----------------------------------------------------------------------------------
# openGraph(height=8,width=12)
png(file=paste0(dirName,"/",fileNameRoot,"-OffCivLatentScaleScatterplot_22.png"), width=12, height=8, units="in", res=300)
layout( matrix( c(1,2), nrow=1 , byrow=TRUE) )
par( mar=c(3.5,2.5,2.5,1) , mgp=c(1.4,0.5,0) , pty="s")
plot(offActContCat$posteriorMedian , civActContCat$posteriorMedian ,
     main="Latent Action Posterior Median Estimates (ForceQ)",
     xlab="Officer Actions",
     ylab="Civilian Actions",
     pch=19,  #as.numeric(offActContCat$offActionName),
     col=plotCols,
     xlim=c(-3,3),
     ylim=c(-3,3),
     mgp=c(1.4,0.5,0)
)
text(offActContCat$posteriorMedian , civActContCat$posteriorMedian ,
     labels=combinedNameVec,
     cex=0.6, pos=posVector)
abline(a=0 , b=1, col="lightgreen", lwd=2)

# Plot of points and HDIs
plot(offActContCat$posteriorMedian , civActContCat$posteriorMedian ,
     main="Posterior Median Estimates and HDIs (ForceQ)",
     xlab="Officer Actions",
     ylab="Civilian Actions",
     pch=19,  #as.numeric(offActContCat$offActionName),
     col=plotCols,
     xlim=c(-3,3),
     ylim=c(-3,3))
abline(a=0 , b=1, col="lightgreen", lwd=2)
segments(x0=offActContCat$HDIlow , y0=civActContCat$posteriorMedian , x1=offActContCat$HDIhigh , y1=civActContCat$posteriorMedian, col="darkgray")
segments(y0=civActContCat$HDIlow , x0=offActContCat$posteriorMedian , y1=civActContCat$HDIhigh , x1=offActContCat$posteriorMedian, col="darkgray")
points(offActContCat$posteriorMedian , civActContCat$posteriorMedian , col=plotCols , pch=19)

combForceContinuum = c("Cooperation, physical proximity, polite dialogue","Strong verbal interaction (e.g., commands or profanity)","Open hand control, defensive resistance, non-deadly\n weapon use with minimal injury potential","Closed hand techniques (e.g., punches or kicks)","Intermediate or aggravated force, non-deadly\n weapon use with moderate injury potential","Deadly force")

# par(mar = c(0,0,0,0))
# plot(c(0, 1), c(0, 1), ann = F, bty = 'n', type = 'n', xaxt = 'n', yaxt = 'n') #plot an empty plot with no annotations, border, no plot type, no x or y axis
plotCols2 =  c("purple","blue","green4","yellow3","orange","red")
xVal = 1.2
yVal = -1
text(x=xVal , y=yVal , labels=bquote(underline("Combined Force Continuum")) , col="black" , cex=0.8 , font=2)
text(x=xVal , y=yVal+-2 , labels=combForceContinuum[1] , col=plotCols2[1] , cex=0.8)
text(x=xVal , y=yVal+-1.67 , labels=combForceContinuum[2] , col=plotCols2[2] , cex=0.8)
text(x=xVal , y=yVal+-1.34 , labels=combForceContinuum[3] , col=plotCols2[3] , cex=0.8)
text(x=xVal , y=yVal+-1.01 , labels=combForceContinuum[4] , col=plotCols2[4] , cex=0.8)
text(x=xVal , y=yVal+-0.68 , labels=combForceContinuum[5] , col=plotCols2[5] , cex=0.8)
text(x=xVal , y=yVal+-0.35 , labels=combForceContinuum[6] , col=plotCols2[6] , cex=0.8)
dev.off()
#Save the graph
# saveGraph( file=paste0(dirName,"/",fileNameRoot,"OffCivLatentScaleScatterplot") , type=graphFileType )


#..................................................
#
# Plot regression coefficients and regression lines
#
#--------------------------------------------------
if(FALSE){
for ( qIdx in 1:max(subData$questionIdx) ) {
  qName = levels(subData$questionName)[qIdx]
  openGraph(height=5,width=10)
  layout( matrix( c(1,2,3, 7, 4,5,6, 8 ) , nrow=2 , byrow=TRUE ) )
  par( mar=c(3.5,0.5,3.5,0.5) , mgp=c(2.25,0.7,0) )
  # 1...
  parName = paste0( "B0[", qIdx , "]" )
  postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
            compVal=0.0 , xlim=c(-1.7,1.0) , border="skyblue" , HDItextPlace = 0.95 )
  B0Mo = postInfo[1,"mode"]
  # 2...
  parName = paste0( "BCivAct[", qIdx , "]" )
  postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
            compVal=0.0 , xlim=c(-1.3,1.3) , border="skyblue", HDItextPlace = 0.95  )
  BCivActMo = postInfo[1,"mode"]
  # 3...
  parName = paste0( "BOffAct[", qIdx , "]" )
  postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
            compVal=0.0 , xlim=c(-1.3,1.3) , border="skyblue", HDItextPlace = 0.95  )
  BOffActMo = postInfo[1,"mode"]
  # 4...
  parName = paste0( "Bfona[", qIdx , "]" )
  postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
            compVal=0.0 , xlim=c(-0.7,0.7) , border="skyblue", HDItextPlace = 0.95  )
  BfonaMo = postInfo[1,"mode"]
  # 5...
  parName = paste0( "BfonaCivAct[", qIdx , "]" )
  postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
            compVal=0.0 , xlim=c(-0.2,0.2) , border="skyblue", HDItextPlace = 0.95  )
  BfonaCivActMo = postInfo[1,"mode"]
  # 6...
  parName = paste0( "BfonaOffAct[", qIdx , "]" )
  postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
            compVal=0.0 , xlim=c(-0.2,0.2) , border="skyblue", HDItextPlace = 0.95  )
  BfonaOffActMo = postInfo[1,"mode"]
  # 7... Plot prediction lines for low fona
  par( mar=c(3.5,3.5,3.5,0.5) , mgp=c(2.25,0.7,0) )
  offActScaleLim=c(-2.5,2.5)
  fonaVal = -2.0
  plot( -99,-99, main=bquote("Pred. Rating,  FONA = "*.(fonaVal)) ,
        xlab="Officer Action Scale Val" , xlim=offActScaleLim ,
        ylim=c(-4.5,4.5) , ylab=paste(qName,"(Logit Rating)") )
  abline(h=0,lty="dotted")
  for ( civActIdx in 1:nActions ) {
    lines( offActScaleLim , 
           B0Mo
           + BCivActMo * civActScaleMo[civActIdx]
           + BOffActMo * offActScaleLim 
           + BfonaMo * fonaVal
           + BfonaCivActMo * civActScaleMo[civActIdx] * fonaVal
           + BfonaOffActMo * offActScaleLim * fonaVal ,
           col=civActIdx )
  }
  # 8... Plot prediction lines for high fona
  offActScaleLim=c(-2.5,2.5)
  fonaVal = 2.0
  plot( -99,-99, main=bquote("Pred. Rating,  FONA = "*.(fonaVal)) ,
        xlab="Officer Action Scale Val" , xlim=offActScaleLim ,
        ylim=c(-4.5,4.5) , ylab=paste(qName,"(Logit Rating)") )
  abline(h=0,lty="dotted")
  for ( civActIdx in 1:nActions ) {
    lines( offActScaleLim , 
           B0Mo
           + BCivActMo * civActScaleMo[civActIdx]
           + BOffActMo * offActScaleLim 
           + BfonaMo * fonaVal
           + BfonaCivActMo * civActScaleMo[civActIdx] * fonaVal
           + BfonaOffActMo * offActScaleLim * fonaVal ,
           col=civActIdx )
  }
  # ...
  saveGraph( file=paste0(dirName,"/",fileNameRoot,"-",qName) , type=graphFileType )
}
} # Closes if/then loop

#---------------------------------------
#
# Plot only the regression coefficients
#
#---------------------------------------
if(FALSE){
  for ( qIdx in 1:max(subData$questionIdx) ) {
    qName = levels(subData$questionName)[qIdx]
    openGraph(height=5,width=10)
    layout( matrix( c(1,2,3,4,5,6) , nrow=2 , byrow=TRUE ) )
    par( mar=c(3.5,0.5,3.5,0.5) , mgp=c(2.25,0.7,0) )
    # 1...
    parName = paste0( "B0[", qIdx , "]" )
    postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
                         compVal=0.0 , xlim=c(-1.7,1.0) , border="skyblue" , HDItextPlace = 0.95 )
    B0Mo = postInfo[1,"mode"]
    # 2...
    parName = paste0( "BCivAct[", qIdx , "]" )
    postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
                         compVal=0.0 , xlim=c(-1.3,1.3) , border="skyblue", HDItextPlace = 0.95  )
    BCivActMo = postInfo[1,"mode"]
    # 3...
    parName = paste0( "BOffAct[", qIdx , "]" )
    postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
                         compVal=0.0 , xlim=c(-1.3,1.3) , border="skyblue", HDItextPlace = 0.95  )
    BOffActMo = postInfo[1,"mode"]
    # 4...
    parName = paste0( "Bfona[", qIdx , "]" )
    postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
                         compVal=0.0 , xlim=c(-0.7,0.7) , border="skyblue", HDItextPlace = 0.95  )
    BfonaMo = postInfo[1,"mode"]
    # 5...
    parName = paste0( "BfonaCivAct[", qIdx , "]" )
    postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
                         compVal=0.0 , xlim=c(-0.2,0.2) , border="skyblue", HDItextPlace = 0.95  )
    BfonaCivActMo = postInfo[1,"mode"]
    # 6...
    parName = paste0( "BfonaOffAct[", qIdx , "]" )
    postInfo = plotPost( mcmcMat[,parName] , main=qName , xlab=parName , 
                         compVal=0.0 , xlim=c(-0.2,0.2) , border="skyblue", HDItextPlace = 0.95  )
    BfonaOffActMo = postInfo[1,"mode"]
    
    saveGraph( file=paste0(dirName,"/",fileNameRoot,"-",qName,"-regCoefs") , type=graphFileType )
  }
}

#------------------------------------------
#
# Plot only the regression prediction lines
#
#------------------------------------------
# plotColorVec = c(rep("purple",4))
plotCols = c("purple","blue","green4","yellow3","orange","red")
plotCols = plotCols[civActContCat$civForceLevel]
plotLines = c(1,2,3,4,5,6)
plotLines = plotLines[civActContCat$civForceLevel]
anchors = c(rep(c("No Force                           Maximum Force"),2))

if(TRUE){
  for ( qIdx in 1:max(subData$questionIdx) ) {
    qName = levels(subData$questionName)[qIdx]
    nActions=22
    # 1... Plot prediction lines for low fona
    openGraph(height=5,width=10)
    # par( mar=c(3.5,6,3.5,0.5) , mgp=c(2.25,0.8,0) , cex.lab=1.5, cex.axis=1.5, cex.main=1.5, cex.sub=1.5, las=1 )
    par( mar=c(3.5,5,3.5,0.5) , mgp=c(2.25,0.8,0) )
    layout( matrix( c(1,2), nrow=1) )
    offActScaleLim=c(-2.5,2.5)
    
    # 1...
    parName = paste0( "B0[", qIdx , "]" )
    postInfo = summaryInfo[parName,]
    B0Mo = postInfo[["Mode"]]
    # 2...
    parName = paste0( "BCivAct[", qIdx , "]" )
    postInfo = summaryInfo[parName,]
    BCivActMo = postInfo[["Mode"]]
    # 3...
    parName = paste0( "BOffAct[", qIdx , "]" )
    postInfo = summaryInfo[parName,]
    BOffActMo = postInfo[["Mode"]]
    # 4...
    parName = paste0( "Bfona[", qIdx , "]" )
    postInfo = summaryInfo[parName,]
    BfonaMo = postInfo[["Mode"]]
    # 5...
    parName = paste0( "BfonaCivAct[", qIdx , "]" )
    postInfo = summaryInfo[parName,]
    BfonaCivActMo = postInfo[["Mode"]]
    # 6...
    parName = paste0( "BfonaOffAct[", qIdx , "]" )
    postInfo = summaryInfo[parName,]
    BfonaOffActMo = postInfo[["Mode"]]
    
    # Plot prediction lines for low fona
    fonaVal = -2.0
    plot( -99,-99, main=bquote("Low Legitimacy ("*.(fonaVal)*")") ,
          xlab="Officer Action Scale Values" , xlim=offActScaleLim , xaxt="n" ,
          ylim=c(-4.5,4.5) , ylab=bquote( atop(
            list( .(paste(qName , "(Logit Rating)") )) ,
            list( .(anchors[qIdx]) ))) )
    axis(1 , at=c(seq(-2.5,2.5,by=1)) , labels=c(1,2,3,4,5,6))
    # Add lines for the estimated officer actions
    segments(x0=offActContCat$posteriorMode,
             y0=-4.8,
             x1=offActContCat$posteriorMode,
             y1=-4.4,
             lty=plotLines,
             col=plotCols)
    # points(x=offActContCat$posteriorMedian , y=rep(-4.6,length(offActContCat$posteriorMedian)) , pch="|" , col=plotCols)
    abline(h=0,lty="dotted")
    for ( civActIdx in 1:nActions ) {
      lines( offActScaleLim , 
             B0Mo
             + BCivActMo * civActContCat$posteriorMode[civActIdx]
             + BOffActMo * offActScaleLim 
             + BfonaMo * fonaVal
             + BfonaCivActMo * civActContCat$posteriorMode[civActIdx] * fonaVal
             + BfonaOffActMo * offActScaleLim * fonaVal ,
             lty=plotLines[civActIdx],
             col=plotCols[civActIdx] )
    }
    # Insert Legend for the line types
    par(xpd=TRUE) # allows the legend to exceed the bounds of the plot frame
    legend("topleft", inset=c(0,-0.08), legend=c(1,2,3,4,5,6), lty=unique(plotLines), col=unique(plotCols), horiz=TRUE, cex=0.8, bty='n')
    par(xpd=FALSE)
    
    # Plot prediction lines for high fona
    offActScaleLim=c(-2.5,2.5)
    fonaVal = 2.0
    plot( -99,-99, main=bquote("High Legitimacy ("*.(fonaVal)*")") ,
          xlab="Officer Action Scale Values" , xlim=offActScaleLim , xaxt="n" ,
          ylim=c(-4.5,4.5) , ylab=bquote( atop(
            list( .(paste(qName , "(Logit Rating)") )) ,
            list( .(anchors[qIdx]) ))) )
    axis(1 , at=c(seq(-2.5,2.5,by=1)) , labels=c(1,2,3,4,5,6))
    # Add lines for the estimated officer actions
    segments(x0=offActContCat$posteriorMode,
             y0=-4.8,
             x1=offActContCat$posteriorMode,
             y1=-4.4,
             lty=plotLines,
             col=plotCols)
    # points(x=offActContCat$posteriorMedian , y=rep(-4.6,length(offActContCat$posteriorMedian)) , pch="|" , col=plotCols)
    abline(h=0,lty="dotted")
    for ( civActIdx in 1:nActions ) {
      lines( offActScaleLim , 
             B0Mo
             + BCivActMo * civActContCat$posteriorMode[civActIdx]
             + BOffActMo * offActScaleLim 
             + BfonaMo * fonaVal
             + BfonaCivActMo * civActContCat$posteriorMode[civActIdx] * fonaVal
             + BfonaOffActMo * offActScaleLim * fonaVal ,
             lty=plotLines[civActIdx],
             col=plotCols[civActIdx] )
    }
    # Insert Legend for the line types
    par(xpd=TRUE) # allows the legend to exceed the bounds of the plot frame
    legend("topleft", inset=c(0,-0.08), legend=c(1,2,3,4,5,6), lty=unique(plotLines), col=unique(plotCols), horiz=TRUE, cex=0.8, bty='n')
    par(xpd=FALSE)
    
    # Save each graph
    saveGraph( file=paste0(dirName,"/",fileNameRoot,"-",qName,"-regLines") , type=graphFileType )
  }
}

#..............................................................................
