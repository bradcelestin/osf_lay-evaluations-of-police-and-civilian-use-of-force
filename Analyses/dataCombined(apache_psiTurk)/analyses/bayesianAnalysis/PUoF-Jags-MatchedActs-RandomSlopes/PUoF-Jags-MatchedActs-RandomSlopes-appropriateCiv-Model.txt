
  model{
  for( rowIdx in 1:nDataRows ) {
  y[rowIdx] ~ dnorm(
  b0[action[rowIdx]] + b1[action[rowIdx]]*fona[rowIdx] ,
  1/Sigma^2)
  yBin[rowIdx] ~ dinterval( y[rowIdx] , threshMat[rowIdx,] )
  } # ends model defining loop
  
  # Priors
  for( pairTypeIdx in 1:nActPairs ) {
  b0[pairTypeIdx] ~ dnorm( b0mu , 1/b0sigma^2 )
  b1[pairTypeIdx] ~ dnorm( b1mu , 1/b1sigma^2 )
  } # ends prior for loop

  # Prior for Sigma
  Sigma ~ dunif( 0 , 5 )

  # Hierarchical priors for b0 and b1  
  b0mu ~ dnorm( 0 , 1/10^2 )
  b0sigma ~ dunif( 0 , 5 )
  b1mu ~ dnorm( 0, 1/10^2 )
  b1sigma ~ dunif( 0 , 5 )

  } # close model string
  
