# README.txt
# Authorship temporarily redacted during blind review process. 
# Do not cite or distribute.

##################################################################################
# General notes & instructions for reproducing analyses reported in the manuscript
##################################################################################
TO REPRODUCE THE ANALYSIS REPORTED IN THE PAPER, RUN THE ENTIRE RUNME.R 
TOP-LEVEL SCRIPT, WHICH WILL EXECUTE A SERIES OF COMPILING AND ANALYSIS SUB-SCRIPTS. 
(Note: due to the complexity of the Bayesian analyses, the RUNME.R script may take 
several hours or more to finish running, depending on your hardware setup.) 

##################################################################################
# Explanation of folder and subfolder structure and contents
##################################################################################

Each subfolder contained in the main OSF folder is described below along with the corresponding R programs that it contains. 

apacheVersion:
  -- analyses/:
      -- dataCleaning_manipCheckTest_SONAReplication.R: this file will read in each of the individual raw SONA data files and compile them into one unified file called offCivRatings.csv, located in the data/processed folder. Additionally, it will check for participants who failed the manipulations checks, exclude them, and save the revised data file containing the remaining participants in a file called mainData.csv, also located in the data/processed folder. 
      -- bayesianAnalysis/PUoF-Jags-ExtendedActions_* scripts: each of these scripts will run a Bayesian analysis on the processed data (i.e., mainData.csv) using different models. These models are described by the names of the .R files. For example, PUoF-Jags-ExtendedActions_SocialMoralQs-Appropriate.R uses a model that includes the acceptability and punishment questions for the officer and civilian, but not (i.e., minus) the appropriateness (or Force) questions, PUoF-Jags-ExtendedActions_SocialMoralQs+NegExp-fona.R includes all six social/moral questions (i.e., acceptability, punishment, and appropriateness for officer and civilian) and also includes negative police experience as an additional predictor while excluding FONA, etc. Each of the corresponding folders in the /bayesianAnalysis/ folder contains the resulting output from the script after which it is named.
      -- data: this folder contains the /raw and /processed data. The raw data has been de-identified so that participant IDs are anonymous, but is otherwise intact as it was originally collected. The processed data is populated by running the dataCleaning_manipCheckTest_SONAReplication.R script described above.
      -- experiment/: this folder contains a version of the javascript experiment that can be run using an Apache server. If one desires to experience the experiment on a local computer, double clicking the exp.html file should open the experiment in a browser window (Note: it will not be able to save data or be accessed remotely without implementing the program using server software). The save_data.php script is included to facilitate saving the data should one desire to run the experiment on a server.

psiTurkVersion:
    -- dataCleaning_extendedActions_psiTurk.R: this file is the pre-processing script for the psiTurk data and functions similarly to the dataCleaning_manipCheckTest_SONAReplication.R listed above. It should be run prior to running the analyses for a complete analaysis reproduction. Importantly, IP addresses have been masked in the raw data by replacing them with random IDs to ensure participant anonymity. The city, state, and country locations for each participant remain available in the raw and processed data. This script removes participants who failed manipulation checks, but also who participated from outside of the US prior to saving the mainData.csv file.
    -- latentActionEstTable.R and regressionCoefModalEstimateTable.R reproduce the LaTeX code used to make the two tables in the paper. See RUNME.R for further details.
    -- polAffCorrelation.R is a script that evaluates the correlation between our single item measure of political affiliation and FONA.
    -- bayesianAnalysis/: this folder contains a series of Bayesian analysis scripts that use different models for the mTurk data as described above.
    -- experiment/: this folder contains a version of the experiment that can be run using psiTurk. The config.txt file is included, but psiTurk must be installed--as well as a method for saving data such as a SQL database--in order for it to function properly.

dataCombined(apache_psiTurk):
    -- analyses/: this folder contains the .R files that created the LaTeX tables in the paper, and the bayesianAnalysis/ folder contains all of the .R scripts that run the Bayesian analyses using different models. The results of the PUoF-Jags-ExtendedActions_ForceQ.R, PUoF-Jags-ExtendedActions_SocialMoralQs.R, and PUoF-Jags-ExtendedActions_ForceVsSocialMoralGraphs.R scripts were those that were reported in the paper.
    -- data/: this folder contains the combined raw and processed data from the SONA and mTurk participants
    -- combiningData.R: this is the preprocessing script that compiles the SONA and mTurk data and should be run after their individual pre-processing scripts (i.e., dataCleaning_manipCheckTest_SONAReplication.R and dataCleaning_extendedActions_psiTurk.R) but before any combined data analysis scripts in order to fully reproduce the analysis.

##################################################################################
# Manually running specific analysis scripts
##################################################################################
If your goal is to reproduce the analyses reported in the paper, the best and easiest strategy is to run the top-leve RUNME.R script in its entirety. However, if you would like to run analyses that are not in the paper, or to reproduce only certain analyses from the paper, please see below.

To run any of the analysis programs manually, double click on the corresponding .R file so that it opens R (or RStudio); this will ensure that the working directory is set properly so that the program can access needed files in the subdirectories (alternatively, ensure that R's working directory is the directory in which the .R analysis file you are trying to run is located). IMPORTANT: all files must be maintained in their relative locations in the directory structure or dependent files and folders won't be available to them.
