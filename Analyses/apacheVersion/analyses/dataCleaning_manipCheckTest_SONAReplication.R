rm(list=ls()) # Careful, this clears all of R's memory
graphics.off() # Closes all open graphics windows

######################################################################
# Check for required packages and install them if they aren't already
######################################################################

want = c("gdata")
have = want %in% rownames(installed.packages())
if ( any(!have) ) { install.packages( want[!have] ) }

############################################
# Define working directory & other variables
############################################
workDir = getwd()
setwd(workDir)

beginTime = proc.time()
fileNames = list.files(path = "../data/raw/", pattern = "*.csv")

header = read.csv(paste0("../data/raw/",fileNames[1]), stringsAsFactors = FALSE, header = TRUE)
colNames = colnames(header)

#Create dataframe to hold info about overall time taken, average time per question, etc.
partInfo = data.frame(matrix(vector(), nrow=0, ncol=5,
                             dimnames=list(c(),c("totalTimeMinutes","meanRTwithoutInstructions","numberOfTrialsMoreThan10seconds","totalNumberOfTrials","partID"))))

instructions = data.frame(matrix(rep(NA, length(colNames)), ncol = length(colNames)))
names(instructions) = colNames
offCivRatings = data.frame(matrix(rep(NA, length(colNames)), ncol = length(colNames)))
names(offCivRatings) = colNames
policeExperience = data.frame(matrix(rep(NA, length(colNames)), ncol = length(colNames)))
names(policeExperience) = colNames
policeLegitimacy = data.frame(matrix(rep(NA, length(colNames)), ncol = length(colNames)))
names(policeLegitimacy) = colNames
demographics = data.frame(matrix(rep(NA, length(colNames)), ncol = length(colNames)))
names(demographics) = colNames
debriefing = data.frame(matrix(rep(NA, length(colNames)), ncol = length(colNames)))
names(debriefing) = colNames

for(fileIdx in 1:length(fileNames)){
  file = read.csv(paste0("../data/raw/", fileNames[fileIdx]), header = TRUE, stringsAsFactors = FALSE)
  
  ##############################################
  # Populate the time taken dataframe for each P
  ##############################################
  part = file$randSubjectID[1] #extract participant ID
  noInst = subset(file, questionType != "instructions" & questionType != "single-stim")
  partInfo[fileIdx,"totalTimeMinutes"] = (tail(file$time_elapsed,1) / 1000) / 60 #total number of minutes
  partInfo[fileIdx, "meanRTwithoutInstructions"] = mean(noInst$rt)
  partInfo[fileIdx, "numberOfTrialsMoreThan10seconds"] = length(file$rt[file$rt>10000])
  partInfo[fileIdx, "totalNumberOfTrials"] = nrow(noInst)
  partInfo[fileIdx, "partID"] = part
  
  #############################################
  # Populate the remaining question dataframes
  #############################################
  inst = file[file$questionType=="instructions",]
  offCiv = file[file$questionType=="offCivRatings",]
  poExp = file[file$questionType=="policeExperience",]
  poLeg = file[file$questionType=="policeLegitimacy",]
  demo = file[file$questionType=="demographics",]
  debr = file[file$questionType=="debriefing",]
  
  instructions = rbind(instructions, inst)
  offCivRatings = rbind(offCivRatings, offCiv)
  policeExperience = rbind(policeExperience, poExp)
  policeLegitimacy = rbind(policeLegitimacy, poLeg)
  demographics = rbind(demographics, demo)
  debriefing = rbind(debriefing, debr)
  
  rm(inst, offCiv, poExp, poLeg, demo, debr, part, noInst, file)
}

# Remove the first row of NA's from each dataframe
instructions = instructions[-1,]
offCivRatings = offCivRatings[-1,]
policeLegitimacy = policeLegitimacy[-1,]
policeExperience = policeExperience[-1,]
demographics = demographics[-1,]
debriefing = debriefing[-1,]

#Convert the character variables to factors
offCivRatings$questionName = as.factor(offCivRatings$questionName)
offCivRatings$civilianAction = as.factor(offCivRatings$civilianAction)
offCivRatings$officerAction = as.factor(offCivRatings$officerAction)
offCivRatings$offActName = as.factor(offCivRatings$offActName)
offCivRatings$civActName = as.factor(offCivRatings$civActName)
offCivRatings$sex = as.factor(offCivRatings$sex)
offCivRatings$polAff = as.factor(offCivRatings$polAff)
offCivRatings$race = as.factor(offCivRatings$race)
offCivRatings$order = as.factor(offCivRatings$order)
offCivRatings$response = as.numeric(offCivRatings$response)

#########################################################
# Calculate manipulation check failures
#########################################################
# allMeanAbsDev = vector() #Create empty vector so you can store all of the MAD scores for later access
partList = unique(offCivRatings$randSubjectID) #Create a vector containing unique participant IDs
manipCheckScores = data.frame(matrix(rep(NA), nrow=length(partList), ncol=4))
failedPs = data.frame(matrix(rep(NA), ncol=4)) #vector for Ps who exceed the thresholds I set for failure
names(failedPs) = c("manipCheckHigh", "manipCheckLow","MADScore","randSubjectID")
names(manipCheckScores) = c("manipCheckHigh", "manipCheckLow","MADScore","randSubjectID")

for(i in 1:length(partList)){
  #Create a variable containing only one individual participant's data at a time
  indvPart = offCivRatings[grep(partList[i], offCivRatings$randSubjectID), ]
  manipCheckScores$randSubjectID[i] = partList[i]
  #Cycle through that participant and remove the manipulation check high and low questions; then check to see if they are at 95 or above and at or below 5 respectively
  for(j in 1:nrow(indvPart)){
    manHigh = indvPart[grep('manipCheckHigh', indvPart$questionName), ]
    flagHigh = 0
    if(nrow(manHigh) > 0){
      for(z in 1:nrow(manHigh)){
        if(manHigh$response[z] <= 95){
          flagHigh = flagHigh + 1
          subIDH = manHigh$randSubjectID[z]
        }
      }
    }
    manLow = indvPart[grep('manipCheckLow', indvPart$questionName), ]
    flagLow = 0
    if(nrow(manLow) > 0){
      for(q in 1:nrow(manLow)){
        if(manLow$response[q] >= 5){
          flagLow = flagLow + 1
          subIDL = manLow$randSubjectID[q]
        }
      }
    }
    manipCheckScores$manipCheckHigh[i] = flagHigh
    manipCheckScores$manipCheckLow[i] = flagLow
  }
  #Create a subset of data that includes the data for one participant without the manipulation check questions
  indvPartNoManip = subset(indvPart, questionType != 'manipCheckHigh' & questionType != 'manipCheckLow')
  mn = mean(indvPartNoManip$response)
  varVect = vector()
  for(varIdx in 1:nrow(indvPartNoManip)){
    varVect[varIdx] = abs(indvPartNoManip$response[varIdx] - mn)
  }
  manipCheckScores$MADScore[i] = round(mean(varVect),3) # Create a vector containing all absolute mean deviation scores
  #--------------------------------------------
  # Check to see if each P failed one of the 
  # manipulation checks and, if so, record their
  # info in failedPs
  #--------------------------------------------
  if(sum(flagLow,flagHigh) > 1 | mean(varVect) <= 15){
    failData = c(flagHigh, flagLow, mean(varVect), partList[i])
    failedPs = rbind(failedPs, failData)
  }
}
failedPs = failedPs[-1,]

#####################################################
# Creat a new column containing aggregate FONA scores
#####################################################
feltObligation = subset(policeLegitimacy, dimension == 'feltObligation')
feltObligation$response = as.numeric(feltObligation$response)
normAlignment = subset(policeLegitimacy, dimension == 'normAlignment')
normAlignment$response = as.numeric(normAlignment$response)

feltObAverage = aggregate(response ~ randSubjectID, data=feltObligation, FUN=mean)
names(feltObAverage) = c('randSubjectID','feltObligation')
normAlignAverage = aggregate(response ~ randSubjectID, data=normAlignment, FUN=mean)
names(normAlignAverage) = c('randSubjectID','normAlignment')

offCivRatings$feltObligation = feltObAverage$feltObligation[match(offCivRatings$randSubjectID, feltObAverage$randSubjectID)]
offCivRatings$normAlignment = normAlignAverage$normAlignment[match(offCivRatings$randSubjectID, normAlignAverage$randSubjectID)]

########################################################################
# Creat a new column containing summed negative police experience scores
########################################################################
expMissingData = which(policeExperience$response=="")
policeExperience = policeExperience[-expMissingData,]
policeExperience$response = factor(policeExperience$response)
negExp = as.numeric(policeExperience$response)-1
policeExperience = cbind(policeExperience, negExp)
policeExperience$randSubjectID = factor(policeExperience$randSubjectID)
negExpSum = aggregate(negExp ~ randSubjectID, data=policeExperience, FUN=sum)
names(negExpSum) = c("randSubjectID","negExpSum")

offCivRatings$negPoExpSum = negExpSum$negExpSum[match(offCivRatings$randSubjectID, negExpSum$randSubjectID)]

# Convert feltObligation and normAlignment to standardized average, called fona:
standardize = function( y ) {
  return( ( y - mean(y) ) / sqrt( mean( (y-mean(y))^2 ) ) )
}
zFO = standardize( offCivRatings$feltObligation )
zNA = standardize( offCivRatings$normAlignment )
fona = ( zFO + zNA ) / 2
offCivRatings = cbind( offCivRatings , fona = fona )

#Write ouf the dataframes to .csv files for safekeeping
write.csv(offCivRatings, file='../data/processed/offCivRatings.csv')
write.csv(demographics, file='../data/processed/demographics.csv')
write.csv(policeExperience, file='../data/processed/policeExperience.csv')
write.csv(policeLegitimacy, file='../data/processed/policeLegitimacy.csv')
write.csv(instructions, file='../data/processed/instructions.csv')
write.csv(partInfo, file='../data/processed/partInfo.csv')
write.csv(failedPs, file='../data/processed/failedPs.csv')

#########################################################################################
# Create a main data file that excludes participants who failed maipulation checks
#########################################################################################
mainData = offCivRatings

# N.B.: internalResp goes from 1 to 100, not from 0 to 100, but is centered at 
# 50. Hence 1 and 99 are symmetric around 50, but responses go from 1 to 100, 
# leaving the logitResp asymmetric. Therefore change responses of 100 to 99,
# so that logit is symmetric. This step is later nullified because we treated
# values more extreme than 5/95 as censored.
mainData[ mainData[,"response"]==100 ,"response"] = 99

# Transform ratings from 0-100 scale to logit scale. First squeeze the ratings
# away from 0/1 extremes, so logit does not explode.
smidge = 0.01
squeezeResp = (1.0-smidge)*(mainData[,"response"]/100) + (smidge/2)
logitResp = log( squeezeResp / ( 1.0 - squeezeResp ) )
mainData = cbind( mainData , logitResp )

#Remove the Ps who failed the manipulation checks as dictated above
mainData = mainData[!mainData$randSubjectID %in% failedPs$randSubjectID,]
write.csv(mainData, file="../data/processed/mainData.csv")

#Print total time required to run the script
endTime = proc.time()
totalTime = endTime - beginTime
print(totalTime)

#Remove all extraneous variables
require(gdata)
rm(list=keep(debriefing, demographics, instructions, offCivRatings, policeExperience, policeLegitimacy, mainData, failedPs, manipCheckScores, partInfo))

