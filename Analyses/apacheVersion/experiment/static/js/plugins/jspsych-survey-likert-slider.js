// Plugin: likert-style slider

/**
 * jspsych-survey-likert
 * a jspsych plugin for measuring items on a likert scale
 *
 * Josh de Leeuw
 *
 * documentation: docs.jspsych.org
 *
 *EXAMPLE USE
 *var scale_1 = ["Strongly Disagree", "Disagree", "Neutral", "Agree", "Strongly Agree"];
 *
 * var likert_block = {
 *       type: 'survey-likert',
 *       questions: ["I like vegetables.", "I like fruit."],
 *       labels: [scale_1, scale_1], // need one scale for every question on a page
 *       intervals: [5,9]
 *   };
 */

jsPsych.plugins['survey-likert'] = (function() {

  var plugin = {};

  plugin.trial = function(display_element, trial) {

    //Create event handler with a value of 0
    var eventHandler = 0;
    
    // if any trial variables are functions
    // this evaluates the function and replaces
    // it with the output of the function
    trial = jsPsych.pluginAPI.evaluateFunctionParameters(trial);

    // show preamble text
    display_element.append($('<div>', {
      "id": 'jspsych-survey-likert-preamble',
      "class": 'jspsych-survey-likert-preamble'
    }));

    $('#jspsych-survey-likert-preamble').html(trial.preamble);

    // add likert scale questions
    for (var i = 0; i < trial.questions.length; i++) {
      // create div
      display_element.append($('<div>', {
        "id": 'jspsych-survey-likert-' + i,
        "class": 'jspsych-survey-likert-question'
      }));

      // add question text
      $("#jspsych-survey-likert-" + i).append('<p class="jspsych-survey-likert-text survey-likert">' + trial.questions[i] + '</p>');

      // create slider
      $("#jspsych-survey-likert-" + i).append($('<div>', {
        "id": 'jspsych-survey-likert-slider-' + i,
        "class": 'jspsych-survey-likert-slider jspsych-survey-likert'
      }));
      $("#jspsych-survey-likert-slider-" + i).slider({
        value: trial.startPosition[i], //Pulls a value from the question in the main html at which to start the slider box for each question //Math.ceil(trial.intervals[i] / 2),
        min: 1,
        max: trial.intervals[i],
        step: 1,
        change: function( event, ui ) {
          (eventHandler = 1, ui = 1);
        }
      });

      // show tick marks
      if (trial.show_ticks) {
        $("#jspsych-survey-likert-" + i).append($('<div>', {
          "id": 'jspsych-survey-likert-sliderticks' + i,
          "class": 'jspsych-survey-likert-sliderticks jspsych-survey-likert',
          "css": {
            "position": 'relative'
          }
        }));
        for (var j = 1; j < trial.intervals[i] - 1; j++) {
          $('#jspsych-survey-likert-slider-' + i).append('<div class="jspsych-survey-likert-slidertickmark"></div>');
        }

        $('#jspsych-survey-likert-slider-' + i + ' .jspsych-survey-likert-slidertickmark').each(function(index) {
          var left = (index + 1) * (100 / (trial.intervals[i] - 1));
          $(this).css({
            'position': 'absolute',
            'left': left + '%',
            'width': '1px',
            'height': '100%',
            'background-color': '#222222'
          });
        });
      }

      // create labels for slider
      $("#jspsych-survey-likert-" + i).append($('<ul>', {
        "id": "jspsych-survey-likert-sliderlabels-" + i,
        "class": 'jspsych-survey-likert-sliderlabels survey-likert',
        "css": {
          "width": "100%",
          "margin": "10px 0px 0px 0px",
          "padding": "0px",
          "display": "inline-block",
          "position": "relative",
          "height": "2em"
        }
      }));

      for (var j = 0; j < trial.labels[i].length; j++) {
        $("#jspsych-survey-likert-sliderlabels-" + i).append('<li>' + trial.labels[i][j] + '</li>');
      }

      // position labels to match slider intervals
      var slider_width = $("#jspsych-survey-likert-slider-" + i).width();
      var num_items = trial.labels[i].length;
      var item_width = slider_width / num_items;
      var spacing_interval = slider_width / (num_items - 1);

      $("#jspsych-survey-likert-sliderlabels-" + i + " li").each(function(index) {
        $(this).css({
          'display': 'inline-block',
          'width': item_width + 'px',
          'margin': '0px',
          'padding': '0px',
          'text-align': 'center',
          'position': 'absolute',
          'left': (spacing_interval * index) - (item_width / 2)
        });
      });
    }

    // add submit button
    display_element.append($('<button>', {
      'id': 'jspsych-survey-likert-next',
      'class': 'jspsych-survey-likert jspsych-btn',
      "css": { // Adding this to the original plugin moves the submit button down so that the labels don't overlap if they are too long
        "position": "relative",
        "top": "45px",
        "left": "325px" // adding this moves the submit button to the center
      }
    }));
    $("#jspsych-survey-likert-next").html('Submit Answer');

    $("#jspsych-survey-likert-next").click(function() {
      if(eventHandler === 0){
        alert("Please respond to the current question before clicking 'Submit Answer'");
      } else {
        // measure response time
        var endTime = (new Date()).getTime();
        var response_time = endTime - startTime;

        // create object to hold responses
        var question_data = {};
        $("div.jspsych-survey-likert-slider").each(function(index) {
          var id = "Q" + index;
          var val = $(this).slider("value");
          var obje = {};
          obje[id] = val;
          $.extend(question_data, obje);
        });

        // save data
        var trial_data = {
          "rt": response_time,
          "responses": JSON.stringify(question_data),
          "internalResp": question_data
        };

        display_element.html('');

        // next trial
        jsPsych.finishTrial(trial_data);
      }
    });

    var startTime = (new Date()).getTime();
  };

  return plugin;
})();
